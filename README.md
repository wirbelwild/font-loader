[![PHP from Packagist](https://img.shields.io/packagist/php-v/bitandblack/font-loader)](http://www.php.net)
[![Latest Stable Version](https://poser.pugx.org/bitandblack/font-loader/v/stable)](https://packagist.org/packages/bitandblack/font-loader)
[![Total Downloads](https://poser.pugx.org/bitandblack/font-loader/downloads)](https://packagist.org/packages/bitandblack/font-loader)
[![License](https://poser.pugx.org/bitandblack/font-loader/license)](https://packagist.org/packages/bitandblack/font-loader)

# Bit&Black Font Loader 

Load fonts, for example from the Google Fonts Library.

## Installation 

This library is made for the use with [Composer](https://packagist.org/packages/bitandblack/font-loader). Add it to your project by running `$ composer require bitandblack/font-loader`. 

## Usage

Create a font request at first. It needs to know the font library you want to use, the font family name and all font styles you want to load:

````php
<?php

use BitAndBlack\FontLoader\FontRequest;
use BitAndBlack\FontLoader\Library\GoogleFonts;

$fontRequest = new FontRequest(
    new GoogleFonts(),
    'Roboto',
    '300',
    '300italic'
);
````

Create a loader instance then and run the process:

````php
<?php

use BitAndBlack\FontLoader\Loader\FileLoader;

$fontLoader = new FileLoader($fontRequest);
$fontLoader->setOutputDir(__DIR__);
$fontLoader->run();
````

## Help 

If you have any questions feel free to contact us under `hello@bitandblack.com`.
