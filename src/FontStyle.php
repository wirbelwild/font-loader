<?php

/**
 * Bit&Black Font Loader.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\FontLoader;

/**
 * Class FontStyle
 *
 * @package BitAndBlack\FontLoader
 */
class FontStyle
{
    /**
     * @var string
     */
    private $fontStyleName;
    
    /**
     * @var string
     */
    private $fontStyleSource;
    
    /**
     * @var int
     */
    private $fontWeight;
    
    /**
     * @var string
     */
    private $fileName;
    
    /**
     * @var string
     */
    private $extension;

    /**
     * FontStyle constructor.
     *
     * @param string $fontStyleName
     * @param string $fontStyleSource
     * @param int $fontWeight
     * @param string $fileName
     * @param string $extension
     */
    public function __construct(
        string $fontStyleName,
        string $fontStyleSource,
        int $fontWeight,
        string $fileName,
        string $extension
    ) {
        $this->fontStyleName = $fontStyleName;
        $this->fontStyleSource = $fontStyleSource;
        $this->fontWeight = $fontWeight;
        $this->fileName = $fileName;
        $this->extension = $extension;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getFontStyleName();
    }

    /**
     * @return string
     */
    public function getFontStyleName(): string
    {
        return $this->fontStyleName;
    }

    /**
     * @return string
     */
    public function getFontStyleSource(): string
    {
        return $this->fontStyleSource;
    }

    /**
     * @return int
     */
    public function getFontWeight(): int
    {
        return $this->fontWeight;
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * @return string
     */
    public function getExtension(): string
    {
        return $this->extension;
    }
}
