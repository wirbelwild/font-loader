<?php

/**
 * Bit&Black Font Loader.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\FontLoader;

use Throwable;

/**
 * Class Exception
 *
 * @package BitAndBlack\FontLoader
 */
class Exception extends \Exception
{
    /**
     * Exception constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message, int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
