<?php

/**
 * Bit&Black Font Loader.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\FontLoader\Library;

use BitAndBlack\FontLoader\FontFormats;
use BitAndBlack\FontLoader\FontStyle;
use BitAndBlack\FontLoader\FontWeights;

/**
 * Class GoogleFonts
 *
 * @package BitAndBlack\FontLoader\Library
 */
class GoogleFonts implements LibraryInterface
{
    /**
     * @var string
     */
    private $libraryUrl = 'https://gwfh.mranftl.com/api/fonts';

    /**
     * @var array<string, array<mixed>>
     */
    private $requestCache = [];

    /**
     * @param string $requestUri
     * @return array<mixed>|null
     */
    private function getRequest(string $requestUri): ?array
    {
        if (array_key_exists($requestUri, $this->requestCache)) {
            return $this->requestCache[$requestUri];
        }

        $fontInformation = @file_get_contents($requestUri);

        if (false === $fontInformation) {
            return null;
        }

        $fontInformation = json_decode($fontInformation, true);
        $this->requestCache[$requestUri] = $fontInformation;
        return $fontInformation;
    }

    /**
     * @inheritDoc
     */
    public function hasFont(string $fontFamily): bool
    {
        $fontFamily = $this->getFontNameEncoded($fontFamily);
        $requestUri = $this->libraryUrl . '/' . $fontFamily;
        return null !== $this->getRequest($requestUri);
    }
    
    /**
     * @inheritDoc
     */
    public function fontHasStyle(string $fontFamily, string $fontStyle): bool
    {
        return [] !== $this->getFontStyles($fontFamily, $fontStyle);
    }
    
    /**
     * @inheritDoc
     */
    public function getFontStyles(string $fontFamily, string $fontStyle): array
    {
        $fontStyles = [];

        $fontFamily = $this->getFontNameEncoded($fontFamily);
        $requestUri = $this->libraryUrl . '/' . $fontFamily;
        $fontInformation = $this->getRequest($requestUri);

        if (null === $fontInformation) {
            return $fontStyles;
        }

        $fontStyleFound = null;

        foreach ($fontInformation['variants'] as $variant) {
            if ($variant['id'] === $fontStyle || $variant['fontWeight'] === $fontStyle) {
                $fontStyleFound = $variant;
                break;
            }
        }

        if (null === $fontStyleFound) {
            return $fontStyles;
        }

        foreach (FontFormats::getFontFormats() as $format) {
            if (!array_key_exists($format, $fontStyleFound)) {
                continue;
            }

            $fontFamily = $fontStyleFound['fontFamily'];
            $fontFamily = trim($fontFamily, '\'');

            $fontWeight = (int) $fontStyleFound['fontWeight'];

            $fontStyle = FontWeights::getFontWeights()[$fontWeight];

            $isItalic = 'italic' === $fontStyleFound['fontStyle'];

            $fontNameFull = $fontFamily . ' ' . $fontStyle;

            if (true === $isItalic) {
                $fontNameFull .= 'Italic';
            }

            $fontNameFullSlug = str_replace(' ', '-', $fontNameFull);

            $fileName = $fontNameFullSlug . '.' . $format;

            $fontSource = $fontStyleFound[$format];

            $fontStyles[] = new FontStyle(
                $fontNameFull,
                $fontSource,
                $fontWeight,
                $fileName,
                $format
            );
        }

        return $fontStyles;
    }

    /**
     * @inheritDoc
     */
    public function getAllFonts(): array
    {
        return $this->getRequest($this->libraryUrl) ?? [];
    }

    /**
     * Returns the encoded font name.
     *
     * @param string $fontFamily
     * @return string
     */
    private function getFontNameEncoded(string $fontFamily): string
    {
        $fontFamily = mb_strtolower($fontFamily);
        return str_replace(' ', '-', $fontFamily);
    }
}
