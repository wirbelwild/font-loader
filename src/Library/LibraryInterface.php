<?php

/**
 * Bit&Black Font Loader.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\FontLoader\Library;

use BitAndBlack\FontLoader\FontStyle;

/**
 * Interface FontLibraryInterface
 *
 * @package BitAndBlack\FontLoader\Library
 */
interface LibraryInterface
{
    /**
     * Returns the whole font list.
     *
     * @return array<string, string>
     */
    public function getAllFonts(): array;
    
    /**
     * Checks of a font exists.
     *
     * @param string $fontFamily
     * @return bool
     */
    public function hasFont(string $fontFamily): bool;

    /**
     * Checks if a font has a specific style.
     *
     * @param string $fontFamily
     * @param string $fontStyle
     * @return bool
     */
    public function fontHasStyle(string $fontFamily, string $fontStyle): bool;

    /**
     * Returns information about a font style.
     *
     * @param string $fontFamily
     * @param string $fontStyle
     * @return array<int, FontStyle>
     */
    public function getFontStyles(string $fontFamily, string $fontStyle): array;
}
