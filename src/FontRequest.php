<?php

/**
 * Bit&Black Font Loader.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\FontLoader;

use BitAndBlack\FontLoader\Exception\FontFamilyNotFoundException;
use BitAndBlack\FontLoader\Exception\FontStyleNotFoundException;
use BitAndBlack\FontLoader\Library\LibraryInterface;

/**
 * Class FontRequest
 *
 * @package BitAndBlack\FontLoader
 */
class FontRequest
{
    /**
     * @var array<int, FontStyle>
     */
    private $fontStyles = [];

    /**
     * FontRequest constructor.
     *
     * @param LibraryInterface $fontLibrary The instance of the library from where you want to download the fonts from.
     * @param string $fontFamily The font family name.
     * @param string ...$fontStyles One or multiple font styles.
     * @throws FontFamilyNotFoundException
     * @throws FontStyleNotFoundException
     */
    public function __construct(LibraryInterface $fontLibrary, string $fontFamily, string ...$fontStyles)
    {
        if (!$fontLibrary->hasFont($fontFamily)) {
            throw new FontFamilyNotFoundException($fontFamily);
        }

        foreach ($fontStyles as $fontStyle) {
            if (!$fontLibrary->fontHasStyle($fontFamily, $fontStyle)) {
                throw new FontStyleNotFoundException($fontFamily, $fontStyle);
            }

            array_push(
                $this->fontStyles,
                ...$fontLibrary->getFontStyles($fontFamily, $fontStyle)
            );
        }
    }
    
    /**
     * @return FontStyle[]
     */
    public function getFontStyles(): array
    {
        return $this->fontStyles;
    }
}
