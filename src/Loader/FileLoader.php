<?php

/**
 * Bit&Black Font Loader.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\FontLoader\Loader;

use BitAndBlack\FontLoader\FontRequest;
use BitAndBlack\FontLoader\FontStyle;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use RuntimeException;

/**
 * Class FileLoader
 *
 * @package BitAndBlack\FontLoader\Loader
 */
class FileLoader implements LoaderInterface, LoggerAwareInterface
{
    /**
     * @var string
     */
    private $outputDir;

    /**
     * @var array<int, FontStyle>
     */
    private $fontStyles;
    
    /**
     * @var LoggerInterface
     */
    private $logger;
    
    /**
     * @var bool
     */
    private $replaceExistingFonts = true;

    /**
     * FontLoader constructor.
     *
     * @param FontRequest $font
     */
    public function __construct(FontRequest $font)
    {
        $this->fontStyles = $font->getFontStyles();
        $this->outputDir = __DIR__;
        $this->logger = new NullLogger();
    }

    /**
     * @param string $outputDir
     * @return FileLoader
     */
    public function setOutputDir(string $outputDir): self
    {
        if (!file_exists($outputDir) && !mkdir($outputDir) && !is_dir($outputDir)) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $outputDir));
        }

        $this->outputDir = $outputDir;
        return $this;
    }

    /**
     * @return bool
     */
    public function run(): bool
    {
        foreach ($this->fontStyles as $fontStyle) {
            $data = @file_get_contents($fontStyle->getFontStyleSource());
            
            if (false === $data) {
                $this->logger->error('Failed to fetch data for font "' . $fontStyle . '".');
                return false;
            }
            
            $this->logger->debug('Loading data for font "' . $fontStyle . '".');
            
            $path = $this->outputDir . DIRECTORY_SEPARATOR . $fontStyle->getFileName();
            
            if (!$this->replaceExistingFonts && file_exists($path)) {
                continue;
            }
            
            file_put_contents($path, $data);
        }
        
        return true;
    }

    /**
     * Sets a logger.
     *
     * @param LoggerInterface $logger
     * @return void
     */
    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }

    /**
     * Disables the replacement of existing fonts.
     *
     * @return $this
     */
    public function disableReplacingExistingFonts(): self
    {
        $this->replaceExistingFonts = false;
        return $this;
    }
}
