<?php

/**
 * Bit&Black Font Loader.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\FontLoader\Loader;

use BitAndBlack\FontLoader\FontRequest;

/**
 * Interface FontLoaderInterface
 *
 * @package BitAndBlack\FontLoader\Loader
 */
interface LoaderInterface
{
    /**
     * FontLoaderInterface constructor.
     *
     * @param FontRequest $font
     */
    public function __construct(FontRequest $font);

    /**
     * @return bool
     */
    public function run(): bool;
}
