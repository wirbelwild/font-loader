<?php

/**
 * Bit&Black Font Loader.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\FontLoader\Exception;

use BitAndBlack\FontLoader\Exception;

/**
 * Class FontStyleNotFoundException
 *
 * @package BitAndBlack\FontLoader\Exception
 */
class FontStyleNotFoundException extends Exception
{
    /**
     * FontStyleNotFoundException constructor.
     *
     * @param string $fontFamily
     * @param string $fontStyle
     */
    public function __construct(string $fontFamily, string $fontStyle)
    {
        parent::__construct('Couldn\'t find style "' . $fontStyle . '" in font "' . $fontFamily . '".');
    }
}
