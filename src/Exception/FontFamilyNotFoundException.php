<?php

/**
 * Bit&Black Font Loader.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\FontLoader\Exception;

use BitAndBlack\FontLoader\Exception;

/**
 * Class FontFamilyNotFoundException
 *
 * @package BitAndBlack\FontLoader\Exception
 */
class FontFamilyNotFoundException extends Exception
{
    /**
     * FontFamilyNotFoundException constructor.
     *
     * @param string $fontFamily
     */
    public function __construct(string $fontFamily)
    {
        parent::__construct('Couldn\'t find font "' . $fontFamily . '".');
    }
}
