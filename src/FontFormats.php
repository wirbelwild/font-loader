<?php

namespace BitAndBlack\FontLoader;

class FontFormats
{
    /**
     * @return array<int, string>
     */
    public static function getFontFormats(): array
    {
        return [
            'eot',
            'svg',
            'ttf',
            'woff',
            'woff2',
        ];
    }
}
