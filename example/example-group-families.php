<?php

use BitAndBlack\FontLoader\Library\GoogleFonts;

require dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

$cacheFile = __DIR__ . DIRECTORY_SEPARATOR . 'cache.json';

if (!file_exists($cacheFile)) {
    $googleFonts = new GoogleFonts();
    $allFonts = $googleFonts->getAllFonts();
    
    file_put_contents(
        $cacheFile,
        json_encode($allFonts, JSON_PRETTY_PRINT)
    );
} else {
    $allFonts = file_get_contents($cacheFile);
    $allFonts = json_decode($allFonts, true);
}

/** @var array<int, string> $fontDescriptionKeyWords */
$fontDescriptionKeyWords = [
    'sans',
    'serif',
    'mono',
    'script',
    'condensed',
    'extended',
    'narrow',
    'wide',
    'display',
    'text',
    'caption',
    'slab',
    'outline',
    'code',
    'sc',
];

/** @var array<int, string> $meaninglessKeyWords */
$meaninglessKeyWords = [
    'pro',
    'one',
    'extra',
];

/** @var array<string, array<int, string>> $allFamilies */
$allFamilies = [];

foreach ($allFonts as $font) {
    $fontName = $font['family'];
    $fontNameParts = explode(' ', $fontName);
    
    foreach ($fontNameParts as $key => $fontNamePart) {
        $fontNamePart = mb_strtolower($fontNamePart);
        
        /** Prevents empty arrays */
        if (0 === $key) {
            continue;
        }
        
        if (in_array($fontNamePart, $fontDescriptionKeyWords, false)) {
            unset($fontNameParts[$key]);
        }
        
        if (in_array($fontNamePart, $meaninglessKeyWords, false)) {
            unset($fontNameParts[$key]);
        }
        
        $numberFormatter = new NumberFormatter(
            'en',
            NumberFormatter::SPELLOUT
        );
        
        $number = (string) $numberFormatter->format($key);

        if ($fontNamePart === $number) {
            unset($fontNameParts[$key]);
        }
        
        if (is_numeric($fontNamePart)) {
            unset($fontNameParts[$key]);
        }
    }

    $fontName = implode(' ', $fontNameParts);
    $allFamilies[$fontName][] = $font['family'];
}

foreach ($allFamilies as &$family) {
    usort(
        $family,
        'strnatcmp'
    );
}

unset($family);

//var_dump($allFamilies);

var_dump(getSortedBySize($allFamilies));

/**
 * @param array $fontFamilies
 * @return array
 */
function getSortedBySize(array $fontFamilies): array
{
    usort(
        $fontFamilies,
        static function ($a, $b) {
            return count($a) < count($b);
        }
    );
    
    return $fontFamilies;
}
