<?php

use BitAndBlack\FontLoader\FontRequest;
use BitAndBlack\FontLoader\Library\GoogleFonts;
use BitAndBlack\FontLoader\Loader\FileLoader;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

require dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

$logger = new Logger('Font Loader Example');
$logger->pushHandler(
    new StreamHandler('php://stdout', Logger::DEBUG)
);

$fontRequest = new FontRequest(
    new GoogleFonts(),
    'Source Sans 3',
    '300',
    '300italic',
    '400'
);

$fontLoader = new FileLoader($fontRequest);
$fontLoader->setLogger($logger);
$fontLoader
    ->setOutputDir(__DIR__ . DIRECTORY_SEPARATOR . 'fonts')
    ->disableReplacingExistingFonts()
    ->run()
;
