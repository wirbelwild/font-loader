<?php

/**
 * Bit&Black Font Loader.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\FontLoader\Tests\Library;

use BitAndBlack\FontLoader\Library\GoogleFonts;
use PHPUnit\Framework\TestCase;

/**
 * Class GoogleFontsTest
 *
 * @package BitAndBlack\FontLoader\Tests\Library
 */
class GoogleFontsTest extends TestCase
{
    /**
     * @todo Improve this poor test
     */
    public function testCanLoadFonts(): void
    {
        $googleFonts = new GoogleFonts();
        $all = $googleFonts->getAllFonts();
        self::assertIsArray($all);
    }
}
