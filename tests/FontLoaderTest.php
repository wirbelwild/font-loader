<?php

/**
 * Bit&Black Font Loader.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\FontLoader\Tests;

use BitAndBlack\FontLoader\Exception\FontFamilyNotFoundException;
use BitAndBlack\FontLoader\Exception\FontStyleNotFoundException;
use BitAndBlack\FontLoader\FontRequest;
use BitAndBlack\FontLoader\Library\GoogleFonts;
use PHPUnit\Framework\TestCase;

/**
 * Class FontLoaderTest
 *
 * @package BitAndBlack\FontLoader
 */
class FontLoaderTest extends TestCase
{
    /**
     * @throws FontFamilyNotFoundException
     * @throws FontStyleNotFoundException
     */
    public function testThrowsExceptionWhenFontFamilyNotFound(): void
    {
        $this->expectException(FontFamilyNotFoundException::class);

        $font = new FontRequest(
            new GoogleFonts(),
            'MISSINGFONT',
            '400'
        );

        unset($font);
    }

    /**
     * @throws FontFamilyNotFoundException
     * @throws FontStyleNotFoundException
     */
    public function testThrowsExceptionWhenFontStyleNotFound(): void
    {
        $this->expectException(FontStyleNotFoundException::class);

        $font = new FontRequest(
            new GoogleFonts(),
            'Roboto',
            'MISSINGSTYLE'
        );

        unset($font);
    }

    /**
     * @throws FontFamilyNotFoundException
     * @throws FontStyleNotFoundException
     */
    public function testLoadsFontWithWhitespace(): void
    {
        $font = new FontRequest(
            new GoogleFonts(),
            'Hind Guntur',
            '400'
        );

        $styles = $font->getFontStyles();

        self::assertNotSame(
            [],
            $styles
        );
    }
}
